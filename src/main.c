#define _GNU_SOURCE
#include <assert.h>
#include <stdio.h>
#include <unistd.h>


#include "mem.h"
#include "mem_internals.h"

#define HEAP_SIZE 4096
#define BLOCK_SIZE 100

static char *test_malloc() {
    void *malloc = _malloc(32);
    if (malloc == NULL) {
        return "no memory allocated";
    }
    return NULL;
}

static void test_one_block_free() {
    void* heap = heap_init(HEAP_SIZE);
    assert(heap != NULL);

    void *block_1 = _malloc(0);
    void *block_2 = _malloc(0);

    _free(block_2);

    struct block_header *block_header_1 = (struct block_header *) (block_1 - offsetof(struct block_header, contents));
    struct block_header *block_header_2 = (struct block_header *) (block_2 - offsetof(struct block_header, contents));

    assert(block_header_1->next == block_header_2);
    assert(block_header_2->is_free);

    _free(block_header_1);
    heap_term();
}

static void test_two_blocks_free() {
    void* heap = heap_init(HEAP_SIZE);
    assert(heap != NULL);

    void* block_1 = _malloc(10);
    void* block_2 = _malloc(20);
    void* block_3 = _malloc(30);

    _free(block_1);
    _free(block_3);

    struct block_header* header2 = (struct block_header *) (block_2 - offsetof(struct block_header, contents));

    assert(header2->is_free == false);

    _free(block_2);
    heap_term();
}

static void test_regions_expansion() {
    void* heap = heap_init(HEAP_SIZE);
    assert(heap != NULL);

    size_t initial_region_size = ((struct region*)heap)->size;

    void* block = _malloc(5 * HEAP_SIZE);
    struct block_header* new_block_header = (struct block_header *) (block - offsetof(struct block_header, contents));
    size_t expanded_region_size = ((struct region*)heap)->size;

    assert(initial_region_size < expanded_region_size);
    assert(new_block_header);
    assert(new_block_header->capacity.bytes >= 5 * HEAP_SIZE);

    _free(block);
    heap_term();
}

static void test_regions_expansion_trouble() {
    void* heap = heap_init(HEAP_SIZE);
    assert(heap != NULL);

    void* hole = mmap(HEAP_START + REGION_MIN_SIZE, BLOCK_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    assert(hole == HEAP_START + REGION_MIN_SIZE);

    void* new_block = _malloc(REGION_MIN_SIZE - offsetof(struct block_header, contents));
    struct block_header* new_block_header =(struct block_header *) (new_block - offsetof(struct block_header, contents));

    assert(new_block != NULL);
    assert(new_block != hole);
    assert(new_block_header->capacity.bytes >= (REGION_MIN_SIZE - offsetof(struct block_header, contents)));

    munmap(hole, BLOCK_SIZE);
    _free(new_block);
    heap_term();
}

int main() {
    test_malloc();
    test_one_block_free();
    test_two_blocks_free();
    test_regions_expansion();
    test_regions_expansion_trouble();
}